# A Blank React Project

## Why?

I made this repo as a way to quickly and easily get started with developing for ReactJS. I know I found the prospect of setting up a wepback server, installing many different pieces of software and trying to get them to all work together quite daunting. This obviously isn't a substitute for jumping in an figuring all this stuff out for yourself, but it is a useful starting point if you quickly want to get developing without the hassle, and can be a good baseline to check against if you're trying to set up some of these tools and can't figure out why they won't work.

## What's included?

I've tried to include all the tools that I think would be useful for a ReactJS application.

- Webpack and Babel, for running the project and transpiling
- Enzyme and Jest, for testing (unit, shallow, functional and snapshot)
- CypressJS for end to end testing
- SASS installed for easier styles
- Prettier and ESLint for linting and code styling
- Redux and Sagas with examples
- TypeScript for type safety
- Git hooks for pre-commit (runs linter and tests)
- StorybookJS to develop components indenpendently, and to act as a UI viewer

## Commands

`yarn install` - install all dependencies

`yarn build` - build the app

`yarn start` - build and watch the app, and launch the dev server

`yarn lint` - run prettier

`yarn test` - run all unit, shallow, snapshot and functional tests

`yarn e2e` - run the e2e testing application

`yarn run storybook` - build and run component library
