describe('Test page renders components', () => {
  beforeEach(() => {
    cy.visit('http://localhost:9000');
  });
  it('Find the components on the page', () => {
    cy.get('.example-class-component');
    cy.get('.example-func-component');
    cy.get('.example-connected-component');
    cy.get('.example-styled-component');
  });
});
