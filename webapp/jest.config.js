module.exports = {
  verbose: false,
  setupFiles: ['./shim.js', './enzyme-setup.js'],
  moduleNameMapper: {
    '\\.(css|scss)$': 'identity-obj-proxy',
  },
  testRegex: '(src/components/.*/tests|src/.*/.*spec)\\.jsx?$',
  roots: ['./src'],
  moduleDirectories: ['node_modules', 'src'],
};
