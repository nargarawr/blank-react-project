import React, { FunctionComponent } from 'react';
import { connect } from 'react-redux';

interface IExampleConnectedComponentProps {
  example: number;
}

export const ExampleConnectedComponent: FunctionComponent<
  IExampleConnectedComponentProps
> = ({ example }) => (
  <div className="example-connected-component">
    I&apos;m a connected component, talking to state ({example})
  </div>
);

const mapStateToProps = state => ({
  example: state.example.example,
});

export default connect(
  mapStateToProps,
  null
)(ExampleConnectedComponent);
