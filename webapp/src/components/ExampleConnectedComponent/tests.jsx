import React from 'react';
import renderer from 'react-test-renderer';

import { ExampleConnectedComponent } from './ExampleConnectedComponent';

describe('ExampleConnectedComponent Snapshots', () => {
  it('should render default correctly', () => {
    const component = renderer
      .create(<ExampleConnectedComponent example={1} />)
      .toJSON();
    expect(component).toMatchSnapshot();
  });
});
