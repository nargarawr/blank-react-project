import React from 'react';

import { storiesOf } from '@storybook/react';

import { ExampleConnectedComponent } from './ExampleConnectedComponent';

storiesOf('ExampleConnectedComponent', module)
  .add('Blank usage', () => <ExampleConnectedComponent example={0} />)
  .add('Default usage', () => <ExampleConnectedComponent example={100} />);
