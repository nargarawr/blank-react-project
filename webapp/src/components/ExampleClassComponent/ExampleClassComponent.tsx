import React, { Component } from 'react';

interface IExampleFuncComponentProps {
  value: number;
}

interface IExampleFuncComponentState {
  value: number;
}

export class ExampleClassComponent extends Component<
  IExampleFuncComponentProps,
  IExampleFuncComponentState
> {
  constructor(props: IExampleFuncComponentProps) {
    super(props);
    this.state = {
      value: 2,
    };
  }

  render() {
    return (
      <div className="example-class-component">
        I have a class, with props ({this.props.value}) and state (
        {this.state.value})
      </div>
    );
  }
}

export default ExampleClassComponent;
