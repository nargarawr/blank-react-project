import React from 'react';

import { storiesOf } from '@storybook/react';

import ExampleClassComponent from './ExampleClassComponent';

storiesOf('ExampleClassComponent', module)
  .add('Blank usage', () => <ExampleClassComponent value={0} />)
  .add('Default usage', () => <ExampleClassComponent value={10} />);
