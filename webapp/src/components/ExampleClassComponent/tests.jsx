import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import ExampleClassComponent from './ExampleClassComponent';

describe('ExampleClassComponent Snapshots', () => {
  it('should render default correctly', () => {
    const component = renderer
      .create(<ExampleClassComponent value={1} />)
      .toJSON();
    expect(component).toMatchSnapshot();
  });

  it('should display the number correctly', () => {
    const value = 10;
    const component = shallow(<ExampleClassComponent value={value} />);
    const expectedText = `I have a class, with props (${value}) and state (2)`;
    expect(component.text()).toBe(expectedText);
  });
});
