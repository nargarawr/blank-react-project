import React, { FunctionComponent } from 'react';

interface IExampleFuncComponentProps {
  value: number;
}

const ExampleFuncComponent: FunctionComponent<IExampleFuncComponentProps> = ({
  value,
}) => (
  <div className="example-func-component">
    I have no class, but have props: {value}
  </div>
);

export default ExampleFuncComponent;
