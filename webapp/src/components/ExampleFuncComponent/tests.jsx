import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import ExampleFuncComponent from './ExampleFuncComponent';

describe('ExampleFuncComponent Snapshots', () => {
  it('should render default correctly', () => {
    const component = renderer
      .create(<ExampleFuncComponent value={1} />)
      .toJSON();
    expect(component).toMatchSnapshot();
  });

  it('should render large numbers correctly', () => {
    const component = renderer
      .create(<ExampleFuncComponent value={99999} />)
      .toJSON();
    expect(component).toMatchSnapshot();
  });
});

describe('ExampleFuncComponent functionality', () => {
  it('should display the number correctly', () => {
    const value = 100;
    const component = shallow(<ExampleFuncComponent value={value} />);

    expect(component.find('.example-func-component').text()).toBe(
      `I have no class, but have props: ${value}`
    );
  });
});
