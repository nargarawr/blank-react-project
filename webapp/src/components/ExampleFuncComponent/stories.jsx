import React from 'react';

import { storiesOf } from '@storybook/react';

import ExampleFuncComponent from './ExampleFuncComponent';

storiesOf('ExampleFuncComponent', module)
  .add('Blank usage', () => <ExampleFuncComponent value={0} />)
  .add('Default usage', () => <ExampleFuncComponent value={10} />);
