import React from 'react';

import { storiesOf } from '@storybook/react';

import ExampleStyledComponent from './ExampleStyledComponent';

storiesOf('ExampleStyledComponent', module).add('Default usage', () => (
  <ExampleStyledComponent />
));
