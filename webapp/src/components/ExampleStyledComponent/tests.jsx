import React from 'react';
import renderer from 'react-test-renderer';

import ExampleStyledComponent from './ExampleStyledComponent';

describe('ExampleStyledComponent Snapshots', () => {
  it('should renders default correctly', () => {
    const component = renderer.create(<ExampleStyledComponent />).toJSON();
    expect(component).toMatchSnapshot();
  });
});
