import React, { FunctionComponent } from 'react';

import './styles.scss';

const ExampleStyledComponent: FunctionComponent = () => (
  <div className="example-styled-component">
    I have no props, but I'm a different
    <span className="example-styled-component--highlight">colour</span>!
  </div>
);

export default ExampleStyledComponent;
