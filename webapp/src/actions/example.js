/* eslint-disable import/prefer-default-export */
import { EXAMPLE_REQUEST } from './types';

export function getExample() {
  return { type: EXAMPLE_REQUEST };
}
