import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import exampleReducer from './reducers/example';
import exampleSaga from './sagas/example';

const initialiseSagaMiddleware = createSagaMiddleware();

const store = createStore(
  combineReducers({
    example: exampleReducer,
  }),
  applyMiddleware(initialiseSagaMiddleware)
);

initialiseSagaMiddleware.run(exampleSaga);

export default store;
