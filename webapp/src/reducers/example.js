/* eslint-disable no-case-declarations */
import {
  EXAMPLE_REQUEST,
  EXAMPLE_REQUEST_SUCCESS,
  EXAMPLE_REQUEST_ERROR,
} from '../actions/types';

const initialState = {
  example: 0,
  fetching: false,
  error: false,
};

export default function itemReducer(state = initialState, action) {
  switch (action.type) {
    case EXAMPLE_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
      });
    case EXAMPLE_REQUEST_SUCCESS:
      return Object.assign({}, state, {
        example: action.payload,
        fetching: false,
        error: false,
      });
    case EXAMPLE_REQUEST_ERROR:
      return Object.assign({}, state, {
        error: true,
        fetching: false,
      });
    default:
      return state;
  }
}
