// eslint-disable-next-line import/no-extraneous-dependencies
import 'regenerator-runtime/runtime';
import { takeEvery, call, put } from 'redux-saga/effects';
import { getExample } from '../providers/ExampleService';
import {
  EXAMPLE_REQUEST,
  EXAMPLE_REQUEST_SUCCESS,
  EXAMPLE_REQUEST_ERROR,
} from '../actions/types';

function* exampleRequest() {
  try {
    const payload = yield call(getExample);
    yield put({ type: EXAMPLE_REQUEST_SUCCESS, payload });
  } catch (e) {
    yield put({ type: EXAMPLE_REQUEST_ERROR, payload: e });
  }
}

export default function* watcherSaga() {
  yield takeEvery(EXAMPLE_REQUEST, exampleRequest);
}
