export const multiplyByThree = (x: number): number => x * 3;

export const multiplyByFive = (x: number): number => x * 5;
