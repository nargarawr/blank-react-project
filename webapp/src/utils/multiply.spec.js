import { multiplyByThree, multiplyByFive } from './multiply.ts';

describe('multiplyByThree', () => {
  it('should triple a number', () => {
    const ans = multiplyByThree(1);
    expect(ans).toBe(3);
  });
});

describe('multiplyByFive', () => {
  it('should quintuple a number', () => {
    const ans = multiplyByFive(1);
    expect(ans).toBe(5);
  });
});
