import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getExample } from './actions/example';
import ExampleClassComponent from './components/ExampleClassComponent/ExampleClassComponent';
import ExampleConnectedComponent from './components/ExampleConnectedComponent/ExampleConnectedComponent';
import ExampleFuncComponent from './components/ExampleFuncComponent/ExampleFuncComponent';
import ExampleStyledComponent from './components/ExampleStyledComponent/ExampleStyledComponent';

interface IAppProps {
  example: number;
  getExample: () => void;
}

interface IAppState {}

export class App extends Component<IAppProps, IAppState> {
  componentDidMount() {
    // The initial state of this.props.example is 0
    // If we run this, we hit the 'api' and set example to be 1
    this.props.getExample();
  }

  render() {
    return (
      <div>
        <div>The value from the store is {this.props.example}</div>
        <ExampleClassComponent value={1} />
        <ExampleFuncComponent value={1} />
        <ExampleConnectedComponent />
        <ExampleStyledComponent />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  example: state.example.example,
});

const mapDispatchToProps = dispatch => ({
  getExample: () => dispatch(getExample()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
